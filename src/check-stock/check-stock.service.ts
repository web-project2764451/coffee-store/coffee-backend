import { Injectable } from '@nestjs/common';
import { UpdateCheckStockDto } from './dto/update-check-stock.dto';
import { CheckStock } from './entities/check-stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Stock } from 'src/stocks/entities/stock.entity';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateCheckStockDto } from './dto/create-check-stock.dto';
import { CheckStockItems } from './entities/check-stock-item.entity';

@Injectable()
export class CheckStockService {
  @InjectRepository(Stock) private stocksRepository: Repository<Stock>;
  @InjectRepository(CheckStock)
  private checkStocksRepository: Repository<CheckStock>;
  @InjectRepository(CheckStockItems)
  private checkStockItemsRepository: Repository<CheckStockItems>;
  @InjectRepository(User) private usersRepository: Repository<User>;

  async create(createCheckStockDto: CreateCheckStockDto) {
    const checkStock = new CheckStock();
    const user = await this.usersRepository.findOneBy({
      id: createCheckStockDto.userId,
    });
    checkStock.employee = user;
    checkStock.employeeId = user.id;
    checkStock.employeeName = user.fullName;
    checkStock.checkStockDate = new Date();
    checkStock.totalUse = 0;
    checkStock.checkStockItems = [];
    for (const sri of createCheckStockDto.checkStockItems) {
      const checkStockItem = new CheckStockItems();
      checkStockItem.stock = await this.stocksRepository.findOneBy({
        id: sri.stockId,
      });
      checkStockItem.name = checkStockItem.stock.name;
      checkStockItem.lastQty = checkStockItem.stock.qty;
      checkStockItem.remain = sri.remain;
      checkStockItem.use = checkStockItem.lastQty - checkStockItem.remain;
      await this.checkStockItemsRepository.save(checkStockItem);
      checkStock.checkStockItems.push(checkStockItem);
      checkStock.totalUse += checkStockItem.use;
    }
    return this.checkStocksRepository.save(checkStock);
  }

  findAll() {
    return this.checkStocksRepository.find({
      relations: {
        checkStockItems: true,
        employee: true,
      },
      order: {
        id: 'ASC',
      },
    });
  }

  findOne(id: number) {
    return this.checkStocksRepository.find({
      where: { id },
      relations: {
        checkStockItems: true,
        employee: true,
      },
    });
  }

  update(id: number, updateCheckStockDto: UpdateCheckStockDto) {
    return `This action updates a #${id} checkStock`;
  }

  remove(id: number) {
    return `This action removes a #${id} checkStock`;
  }
}
