import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { CheckStockItems } from './check-stock-item.entity';

@Entity()
export class CheckStock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  checkStockDate: Date;

  @Column()
  employeeName: string;

  @Column()
  employeeId: number;

  @Column()
  totalUse: number;

  @ManyToOne(() => User, (employee) => employee.checkStocks)
  employee: User;

  @OneToMany(
    () => CheckStockItems,
    (stockReceiptItem) => stockReceiptItem.checkStock,
  )
  checkStockItems: CheckStockItems[];
}
