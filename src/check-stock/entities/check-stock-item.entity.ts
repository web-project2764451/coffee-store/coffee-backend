import { Stock } from 'src/stocks/entities/stock.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CheckStock } from './check-stock.entity';

@Entity()
export class CheckStockItems {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  lastQty: number;

  @Column()
  use: number;

  @Column()
  remain: number;

  @ManyToOne(() => Stock, (stock) => stock.stockReceiptItems)
  stock: Stock;

  @ManyToOne(() => CheckStock, (checkStock) => checkStock.checkStockItems, {
    onDelete: 'CASCADE',
  })
  checkStock: CheckStock;
}
