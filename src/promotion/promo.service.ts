import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promo.dto';
import { UpdatePromotionDto } from './dto/update-promo.dto';
import { Repository } from 'typeorm';
import { Promotion } from './entities/promo.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PromotionService {
  constructor(
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
  ) {}

  async create(createPromotionDto: CreatePromotionDto) {
    return this.promotionRepository.save(createPromotionDto);
  }

  async findAll() {
    return this.promotionRepository.find();
  }

  async findOne(id: number) {
    return this.promotionRepository.findOneOrFail({ where: { id } });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    await this.promotionRepository.update(id, updatePromotionDto);
    return this.promotionRepository.findOne({ where: { id } });
  }

  async remove(id: number) {
    const promotionToRemove = await this.promotionRepository.findOneOrFail({
      where: { id },
    });
    return this.promotionRepository.remove(promotionToRemove);
  }
}
