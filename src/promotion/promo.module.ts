import { Module } from '@nestjs/common';
import { PromotionService } from './promo.service';
import { PromotionController } from './promo.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Promotion } from './entities/promo.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Promotion])],
  controllers: [PromotionController],
  providers: [PromotionService],
  exports: [PromotionService],
})
export class PromotionModule {}
