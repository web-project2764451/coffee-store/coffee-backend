import { PartialType } from '@nestjs/mapped-types';
import { CreatePromotionDto } from './create-promo.dto';

export class UpdatePromotionDto extends PartialType(CreatePromotionDto) {}
