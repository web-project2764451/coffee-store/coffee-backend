export class CreatePromotionDto {
  name: string;
  condition: string;
  discount: number;
  discountType: string;
  start: string;
  end: string;
  rating: number;
  status: boolean;
}
