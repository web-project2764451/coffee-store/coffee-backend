import { Module } from '@nestjs/common';
import { SalarysService } from './salarys.service';
import { SalarysController } from './salarys.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Salary } from './entities/salary.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Salary, User])],
  controllers: [SalarysController],
  providers: [SalarysService],
  exports: [SalarysService],
})
export class SalarysModule {}
