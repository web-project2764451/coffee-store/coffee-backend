import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private salarysRepository: Repository<Salary>,
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}
  async create(createSalaryDto: CreateSalaryDto) {
    const salary = new Salary();
    const user = await this.usersRepository.findOneBy({
      id: createSalaryDto.employeeId,
    });
    salary.employee = user;
    salary.employeeId = user.id;
    salary.employeeName = user.fullName;
    salary.bankAcc = createSalaryDto.bankAcc;
    salary.baseSalary = 20000;
    salary.increaseSalary = createSalaryDto.increaseSalary;
    salary.salaryReduc = createSalaryDto.salaryReduc;
    salary.netSalary =
      createSalaryDto.baseSalary +
      createSalaryDto.increaseSalary -
      createSalaryDto.salaryReduc;
    salary.status = 'Pending';
    return this.salarysRepository.save(salary);
  }

  findAll() {
    return this.salarysRepository.find({
      relations: {
        employee: true,
      },
      order: {
        id: 'ASC',
      },
    });
  }

  findOne(id: number) {
    return this.salarysRepository.findOne({
      where: { id },
      relations: {
        employee: true,
      },
    });
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    const salary = await this.salarysRepository.findOneOrFail({
      where: { id },
    });
    salary.payDate = new Date();
    salary.bankAcc = updateSalaryDto.bankAcc;
    salary.baseSalary = updateSalaryDto.baseSalary;
    salary.increaseSalary = updateSalaryDto.increaseSalary;
    salary.salaryReduc = updateSalaryDto.salaryReduc;
    if (salary.increaseSalary === undefined) {
      salary.netSalary =
        updateSalaryDto.baseSalary +
        salary.increaseSalary -
        updateSalaryDto.salaryReduc;
    } else if (salary.salaryReduc === undefined) {
      salary.netSalary =
        updateSalaryDto.baseSalary +
        updateSalaryDto.increaseSalary -
        salary.salaryReduc;
    } else {
      salary.netSalary =
        updateSalaryDto.baseSalary +
        updateSalaryDto.increaseSalary -
        updateSalaryDto.salaryReduc;
    }
    salary.status = updateSalaryDto.status;
    await this.salarysRepository.save(salary);
    const result = await this.salarysRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const delSalary = await this.salarysRepository.findOneOrFail({
      where: { id },
    });
    await this.salarysRepository.remove(delSalary);
    return delSalary;
  }
}
