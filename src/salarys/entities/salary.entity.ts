import { User } from 'src/users/entities/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (employee) => employee.salarys)
  employee: User;

  @Column()
  employeeId: number;

  @Column()
  employeeName: string;

  @Column({ nullable: true })
  payDate: Date;

  @Column()
  bankAcc: string;

  @Column()
  baseSalary: number;

  @Column()
  increaseSalary: number;

  @Column()
  salaryReduc: number;

  @Column()
  netSalary: number;

  @Column()
  status: string;
}
