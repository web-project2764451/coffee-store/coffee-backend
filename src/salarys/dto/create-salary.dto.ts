import { User } from 'src/users/entities/user.entity';

export class CreateSalaryDto {
  employee: User[];
  employeeId: number;
  payDate: Date;
  bankAcc: string;
  baseSalary: number;
  increaseSalary: number;
  salaryReduc: number;
  netSalary: number;
  status: string;
}
