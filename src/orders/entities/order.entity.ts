import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Promotion } from 'src/promotion/entities/promo.entity';
import { Member } from 'src/member/entities/member.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  totalBefore: number;

  @Column()
  memberDiscount: number;

  @Column()
  total: number;

  @Column({ nullable: true }) // Set nullable to true
  amount?: number;

  @Column()
  qty: number;

  @Column()
  change: number;

  @Column()
  paymentType: string;

  @Column({ nullable: true }) // Set nullable to true
  memberId: number;

  @Column()
  userId: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  promoriton?: Promotion;

  member?: Member; // Declare member as nullable

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  receiptItems: OrderItem[];

  @ManyToOne(() => User, (user) => user.orders)
  user: User;
}
