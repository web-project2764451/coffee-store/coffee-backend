export class CreateOrderDto {
  orderItems: {
    category: string;
    sweet: number;
    productId: number;
    qty: number;
  }[];
  memberId: number;
  userId: number;
  memberDiscount: number;
  change: number;
  paymentType: string;
  amount: number;
  totalBefore: number;
  total: number;
}
