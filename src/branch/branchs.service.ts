import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Branch } from './entities/branch.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BranchsService {
  constructor(
    @InjectRepository(Branch) private branchsRepository: Repository<Branch>,
  ) {}
  create(createBranchDto: CreateBranchDto) {
    const branch = new Branch();
    branch.branchName = createBranchDto.branchName;
    branch.address = createBranchDto.address;
    return this.branchsRepository.save(branch);
  }

  findAll() {
    return this.branchsRepository.find();
  }

  findOne(id: number) {
    return this.branchsRepository.findOne({ where: { id } });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    const updateBranch = await this.branchsRepository.findOneOrFail({
      where: { id },
    });
    updateBranch.branchName = updateBranchDto.branchName;
    updateBranch.address = updateBranchDto.address;
    await this.branchsRepository.save(updateBranch);

    const result = await this.branchsRepository.findOne({
      where: { id },
    });

    return result;
  }

  async remove(id: number) {
    const removedBranch = await this.branchsRepository.findOneByOrFail({ id });
    await this.branchsRepository.remove(removedBranch);
    return removedBranch;
  }
}
