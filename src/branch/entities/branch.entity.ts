// import { User } from 'src/users/entities/user.entity';
import { Expenses } from 'src/expensess/entities/expenses.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  // OneToMany,
} from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  branchName: string;

  @Column()
  address: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => Expenses, (expenses) => expenses.branch)
  expenses: Expenses[];

  // @OneToMany(() => User, (user) => user.branch)
  // branchs: Branch[];
}
