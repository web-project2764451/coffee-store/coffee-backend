import { IsNotEmpty, Length } from 'class-validator';
export class CreateMemberDto {
  @IsNotEmpty()
  @Length(4, 32)
  name: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;
}
