import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MembersService {
  constructor(
    @InjectRepository(Member) private membersRepository: Repository<Member>,
  ) {}
  create(createMemberDto: CreateMemberDto) {
    return this.membersRepository.save(createMemberDto);
  }

  findAll() {
    return this.membersRepository.find();
  }

  findOne(id: number) {
    return this.membersRepository.findOneBy({ id });
  }

  async findOneByTel(tel: string) {
    return await this.membersRepository.findOneBy({ tel });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    await this.membersRepository.findOneByOrFail({ id });
    await this.membersRepository.update(id, updateMemberDto);
    const updatedMember = await this.membersRepository.findOneBy({ id });
    return updatedMember;
  }

  async remove(id: number) {
    const removedMember = await this.membersRepository.findOneByOrFail({ id });
    await this.membersRepository.remove(removedMember);
    return removedMember;
  }
}
