import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { ProductsModule } from './products/products.module';
import { UsersModule } from './users/users.module';
import { OrdersModule } from './orders/orders.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { Type } from './types/entities/type.entity';
import { Product } from './products/entities/product.entity';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { DataSource } from 'typeorm';
import { AuthModule } from './auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Member } from './member/entities/member.entity';
import { MembersModule } from './member/members.module';
import { StocksModule } from './stocks/stocks.module';
import { Stock } from './stocks/entities/stock.entity';
import { StockReceipt } from './stock-receipt/entities/stock-receipt.entity';
import { StockReceiptItems } from './stock-receipt/entities/stock-receipt-items.entity';
import { StockReceiptModule } from './stock-receipt/stock-receipt.module';
import { Expenses } from './expensess/entities/expenses.entity';
import { ExpensessModule } from './expensess/expensess.module';
import { HistoryEntity } from './history/entities/history.entity';
import { HistoryModule } from './history/history.module';
import { Salary } from './salarys/entities/salary.entity';
import { SalarysModule } from './salarys/salarys.module';
import { PromotionModule } from './promotion/promo.module';
import { Promotion } from './promotion/entities/promo.entity';
import { CheckStockModule } from './check-stock/check-stock.module';
import { CheckStock } from './check-stock/entities/check-stock.entity';
import { CheckStockItems } from './check-stock/entities/check-stock-item.entity';
import { Branch } from './branch/entities/branch.entity';
import { BranchsModule } from './branch/branchs.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      synchronize: true,
      logging: false,
      entities: [
        User,
        Role,
        Type,
        Product,
        Order,
        OrderItem,
        Member,
        Stock,
        StockReceipt,
        StockReceiptItems,
        Salary,
        Expenses,
        HistoryEntity,
        Promotion,
        CheckStock,
        CheckStockItems,
        Branch,
      ],
    }),
    TypeOrmModule.forFeature([HistoryEntity]),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    RolesModule,
    TypesModule,
    MembersModule,
    ProductsModule,
    UsersModule,
    OrdersModule,
    AuthModule,
    StocksModule,
    StockReceiptModule,
    SalarysModule,
    ExpensessModule,
    HistoryModule,
    PromotionModule,
    CheckStockModule,
    BranchsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
