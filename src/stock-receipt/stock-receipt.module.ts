import { Module } from '@nestjs/common';
import { StockReceiptService } from './stock-receipt.service';
import { StockReceiptController } from './stock-receipt.controller';
import { Stock } from 'src/stocks/entities/stock.entity';
import { StockReceipt } from './entities/stock-receipt.entity';
import { StockReceiptItems } from './entities/stock-receipt-items.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Stock, StockReceipt, StockReceiptItems, User]),
  ],
  controllers: [StockReceiptController],
  providers: [StockReceiptService],
})
export class StockReceiptModule {}
