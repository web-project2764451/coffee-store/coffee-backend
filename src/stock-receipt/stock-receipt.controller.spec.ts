import { Test, TestingModule } from '@nestjs/testing';
import { StockReceiptController } from './stock-receipt.controller';
import { StockReceiptService } from './stock-receipt.service';

describe('StockReceiptController', () => {
  let controller: StockReceiptController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockReceiptController],
      providers: [StockReceiptService],
    }).compile();

    controller = module.get<StockReceiptController>(StockReceiptController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
