import { Stock } from 'src/stocks/entities/stock.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { StockReceipt } from './stock-receipt.entity';

@Entity()
export class StockReceiptItems {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  qty: number;

  @Column()
  total: number;

  @ManyToOne(() => Stock, (stock) => stock.stockItems)
  stock: Stock;

  @ManyToOne(
    () => StockReceipt,
    (stockReceipt) => stockReceipt.orderReceiptItem,
    {
      onDelete: 'CASCADE',
    },
  )
  stockReceipt: StockReceipt;
}
