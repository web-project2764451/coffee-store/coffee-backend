import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { StockReceiptItems } from './stock-receipt-items.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class StockReceipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  receiptDate: Date;

  @Column()
  totalQty: number;

  @Column()
  totalPrice: number;

  @OneToMany(
    () => StockReceiptItems,
    (stockReceiptItem) => stockReceiptItem.stockReceipt,
  )
  orderReceiptItem: StockReceiptItems[];

  @ManyToOne(() => User, (employee) => employee.stockReceipts)
  employee: User;

  @Column()
  employeeName: string;
  @Column()
  employeeId: number;
}
