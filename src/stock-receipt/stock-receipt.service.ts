import { Injectable } from '@nestjs/common';
import { CreateStockReceiptDto } from './dto/create-stock-receipt.dto';
import { UpdateStockReceiptDto } from './dto/update-stock-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Stock } from 'src/stocks/entities/stock.entity';
import { Repository } from 'typeorm';
import { StockReceipt } from './entities/stock-receipt.entity';
import { StockReceiptItems } from './entities/stock-receipt-items.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class StockReceiptService {
  @InjectRepository(Stock) private stocksRepository: Repository<Stock>;
  @InjectRepository(StockReceipt)
  private stockReceiptsRepository: Repository<StockReceipt>;
  @InjectRepository(StockReceiptItems)
  private stockReceiptItemsRepository: Repository<StockReceiptItems>;
  @InjectRepository(User) private usersRepository: Repository<User>;

  async create(createStockReceiptDto: CreateStockReceiptDto) {
    const stockReceipt = new StockReceipt();
    const user = await this.usersRepository.findOneBy({
      id: createStockReceiptDto.userId,
    });
    stockReceipt.employee = user;
    stockReceipt.employeeId = user.id;
    stockReceipt.employeeName = user.fullName;
    stockReceipt.receiptDate = new Date();
    stockReceipt.totalPrice = 0;
    stockReceipt.totalQty = 0;
    stockReceipt.orderReceiptItem = [];
    for (const sri of createStockReceiptDto.stockReceiptItems) {
      const stockReceiptItem = new StockReceiptItems();
      stockReceiptItem.stock = await this.stocksRepository.findOneBy({
        id: sri.stockId,
      });
      stockReceiptItem.name = stockReceiptItem.stock.name;
      stockReceiptItem.price = sri.priceUnit;
      stockReceiptItem.qty = sri.qty;
      stockReceiptItem.total = stockReceiptItem.price * stockReceiptItem.qty;
      await this.stockReceiptItemsRepository.save(stockReceiptItem);
      stockReceipt.orderReceiptItem.push(stockReceiptItem);
      stockReceipt.totalPrice += stockReceiptItem.total;
      stockReceipt.totalQty += stockReceiptItem.qty;
    }
    return this.stockReceiptsRepository.save(stockReceipt);
  }

  findAll() {
    return this.stockReceiptsRepository.find({
      relations: {
        orderReceiptItem: true,
        employee: true,
      },
      order: {
        id: 'ASC',
      },
    });
  }

  findOne(id: number) {
    return this.stockReceiptsRepository.find({
      where: { id },
      relations: {
        orderReceiptItem: true,
        employee: true,
      },
    });
  }

  update(id: number, updateStockReceiptDto: UpdateStockReceiptDto) {
    return `This action updates a #${id} stockReceipt`;
  }

  remove(id: number) {
    return `This action removes a #${id} stockReceipt`;
  }
}
