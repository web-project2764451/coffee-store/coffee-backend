import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { StockReceiptService } from './stock-receipt.service';
import { CreateStockReceiptDto } from './dto/create-stock-receipt.dto';
import { UpdateStockReceiptDto } from './dto/update-stock-receipt.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('stock-receipt')
export class StockReceiptController {
  constructor(private readonly stockReceiptService: StockReceiptService) {}

  @Post()
  create(@Body() createStockReceiptDto: CreateStockReceiptDto) {
    return this.stockReceiptService.create(createStockReceiptDto);
  }

  @Get()
  findAll() {
    return this.stockReceiptService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.stockReceiptService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateStockReceiptDto: UpdateStockReceiptDto,
  ) {
    return this.stockReceiptService.update(+id, updateStockReceiptDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.stockReceiptService.remove(+id);
  }
}
