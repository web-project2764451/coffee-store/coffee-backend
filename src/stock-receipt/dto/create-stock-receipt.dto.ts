export class CreateStockReceiptDto {
  stockReceiptItems: {
    stockId: number;
    qty: number;
    priceUnit: number;
  }[];
  userId: number;
}
