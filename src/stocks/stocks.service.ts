import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock) private stocksRepository: Repository<Stock>,
  ) {}
  create(createStockDto: CreateStockDto) {
    const stock = new Stock();
    stock.name = createStockDto.name;
    stock.qty = createStockDto.qty;
    stock.priceUnit = createStockDto.priceUnit;
    stock.value = createStockDto.qty * createStockDto.priceUnit;
    return this.stocksRepository.save(stock);
  }

  findAll() {
    return this.stocksRepository.find();
  }

  findOne(id: number) {
    return this.stocksRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    const stock = await this.stocksRepository.findOneOrFail({
      where: { id },
    });
    const priceUnit = stock.priceUnit;
    const qty = stock.qty;
    stock.name = updateStockDto.name;
    stock.qty = updateStockDto.qty;
    stock.priceUnit = updateStockDto.priceUnit;
    if (stock.qty === undefined) {
      stock.value = updateStockDto.priceUnit * qty;
    } else if (stock.priceUnit === undefined) {
      stock.value = priceUnit * updateStockDto.qty;
    } else {
      stock.value = updateStockDto.priceUnit * updateStockDto.qty;
    }
    await this.stocksRepository.save(stock);
    const result = await this.stocksRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteStock = await this.stocksRepository.findOneOrFail({
      where: { id },
    });
    await this.stocksRepository.remove(deleteStock);
    return deleteStock;
  }
}
