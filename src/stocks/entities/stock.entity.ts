import { StockReceiptItems } from 'src/stock-receipt/entities/stock-receipt-items.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  qty: number;

  @Column()
  priceUnit: number;

  @Column()
  value: number;

  @OneToMany(
    () => StockReceiptItems,
    (stockReceiptItems) => stockReceiptItems.stock,
  )
  stockItems: StockReceiptItems[];

  @OneToMany(
    () => StockReceiptItems,
    (stockReceiptItems) => stockReceiptItems.stock,
  )
  stockReceiptItems: StockReceiptItems[];
}
