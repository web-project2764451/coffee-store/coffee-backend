export class CreateStockDto {
  name: string;
  qty: number;
  priceUnit: number;
  value: number;
}
