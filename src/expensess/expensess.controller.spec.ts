import { Test, TestingModule } from '@nestjs/testing';
import { ExpensessController } from './expensess.controller';
import { ExpensessService } from './expensess.service';

describe('ExpensessController', () => {
  let controller: ExpensessController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExpensessController],
      providers: [ExpensessService],
    }).compile();

    controller = module.get<ExpensessController>(ExpensessController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
