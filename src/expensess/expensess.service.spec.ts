import { Test, TestingModule } from '@nestjs/testing';
import { ExpensessService } from './expensess.service';

describe('ExpensessService', () => {
  let service: ExpensessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExpensessService],
    }).compile();

    service = module.get<ExpensessService>(ExpensessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
