import { Branch } from 'src/branch/entities/branch.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Expenses {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Branch, (branch) => branch.expenses)
  branch: Branch;

  @Column()
  branchId: number;

  @Column()
  branchName: string;

  @Column()
  billType: string;

  @Column()
  totalAmount: number;

  expenses: Branch;
}
