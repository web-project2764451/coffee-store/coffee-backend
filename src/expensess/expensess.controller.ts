import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ExpensessService } from './expensess.service';
import { CreateExpensesDto } from './dto/create-expenses.dto';
import { UpdateExpensesDto } from './dto/update-expenses.dto';

@Controller('expensess')
export class ExpensessController {
  constructor(private readonly expensessService: ExpensessService) {}

  @Post()
  create(@Body() createExpensesDto: CreateExpensesDto) {
    return this.expensessService.create(createExpensesDto);
  }

  @Get()
  findAll() {
    return this.expensessService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.expensessService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateExpensesDto: UpdateExpensesDto,
  ) {
    return this.expensessService.update(+id, updateExpensesDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.expensessService.remove(+id);
  }
}
