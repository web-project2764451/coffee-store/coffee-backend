import { Injectable } from '@nestjs/common';
import { CreateExpensesDto } from './dto/create-expenses.dto';
import { UpdateExpensesDto } from './dto/update-expenses.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Expenses } from './entities/expenses.entity';
import { Repository } from 'typeorm';
import { Branch } from 'src/branch/entities/branch.entity';

@Injectable()
export class ExpensessService {
  constructor(
    @InjectRepository(Expenses)
    private expensessRepository: Repository<Expenses>,
    @InjectRepository(Branch)
    private branchsRepository: Repository<Branch>,
  ) {}
  async create(createExpensesDto: CreateExpensesDto) {
    const expenses = new Expenses();
    const branch = await this.branchsRepository.findOneBy({
      id: createExpensesDto.branchId,
    });
    expenses.branch = branch;
    expenses.branchName = branch.branchName;
    expenses.billType = createExpensesDto.billType;
    expenses.totalAmount = createExpensesDto.totalAmount;
    return this.expensessRepository.save(expenses);
  }

  findAll() {
    return this.expensessRepository.find({
      relations: {
        branch: true,
      },
    });
  }

  findOne(id: number) {
    return this.expensessRepository.findOne({
      where: { id },
      relations: {
        branch: true,
      },
    });
  }

  async update(id: number, updateExpensesDto: UpdateExpensesDto) {
    const updateExpenses = await this.expensessRepository.findOneOrFail({
      where: { id },
    });
    updateExpenses.billType = updateExpensesDto.billType;
    updateExpenses.totalAmount = updateExpensesDto.totalAmount;
    await this.expensessRepository.save(updateExpenses);

    const result = await this.expensessRepository.findOne({
      where: { id },
    });
    return result;
  }

  remove(id: number) {
    return `This action removes a #${id} Expenses`;
  }
}
