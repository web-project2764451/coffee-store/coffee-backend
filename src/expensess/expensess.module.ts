import { Module } from '@nestjs/common';
import { ExpensessService } from './expensess.service';
import { ExpensessController } from './expensess.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Expenses } from './entities/expenses.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Expenses, Branch])],
  controllers: [ExpensessController],
  providers: [ExpensessService],
  exports: [ExpensessService],
})
export class ExpensessModule {}
