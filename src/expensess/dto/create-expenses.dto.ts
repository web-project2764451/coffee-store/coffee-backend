import { Branch } from 'src/branch/entities/branch.entity';

export class CreateExpensesDto {
  branchId: number;
  branchName: string;
  branch: Branch[];
  billType: string;
  totalAmount: number;
}
