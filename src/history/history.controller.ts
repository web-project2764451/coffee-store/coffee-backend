import { Controller, Delete, Get, Post } from '@nestjs/common';
import { HistoryService } from './history.service';

@Controller()
export class HistoryController {
  constructor(private historyService: HistoryService) {}

  @Get('history')
  async getHistory() {
    return await this.historyService.getHistory();
  }

  @Post('clock-in')
  async clockIn() {
    return await this.historyService.clockIn();
  }

  @Post('clock-out')
  async clockOut() {
    return await this.historyService.clockOut();
  }

  @Delete('history/clear')
  async clearHistory() {
    await this.historyService.clearHistory();
    return { message: 'History cleared successfully' };
  }
}
