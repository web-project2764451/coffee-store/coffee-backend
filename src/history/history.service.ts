import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HistoryEntity } from './entities/history.entity';

@Injectable()
export class HistoryService {
  constructor(
    @InjectRepository(HistoryEntity)
    private historyRepository: Repository<HistoryEntity>,
  ) {}

  async getHistory(): Promise<HistoryEntity[]> {
    return await this.historyRepository.find();
  }

  async clockIn(): Promise<HistoryEntity> {
    const date = new Date();
    const time = `${this.formatTime(date)}   ${this.formatDate(date)}`;

    const historyItem = this.historyRepository.create({ type: 'in', time });
    return await this.historyRepository.save(historyItem);
  }

  async clockOut(): Promise<HistoryEntity> {
    const date = new Date();
    const time = `${this.formatTime(date)}   ${this.formatDate(date)}`;

    const historyItem = this.historyRepository.create({ type: 'out', time });
    return await this.historyRepository.save(historyItem);
  }

  async clearHistory() {
    await this.historyRepository.clear(); // ลบทั้งหมดในฐานข้อมูล
  }

  private formatDate(date: Date): string {
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  }

  private formatTime(date: Date): string {
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    return `${hours}:${minutes}`;
  }
}
